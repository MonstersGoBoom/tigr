
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "tigr.h"
#include "image.h"
//Copyright (c) 2019 MonstersGoBoom
//bmp / image utils 

#pragma pack(1)
typedef struct
{
	uint16_t magic;
	uint32_t fileSize;
	uint32_t reserved0;
	uint32_t bitmapDataOffset;
	uint32_t bitmapHeaderSize;
	uint32_t width;
	uint32_t height;
	uint16_t planes;
	uint16_t bitsPerPixel;
	uint32_t compression;
	uint32_t bitmapDataSize;
	uint32_t hRes;
	uint32_t vRes;
	uint32_t colors;
	uint32_t importantColors;
} BMPHeader_t;

void freeimage(image_t *image)
{
	free(image->buffer);
	free(image->colors);
}

Tigr *tigrLoadBMP(const char *fname)
{


	uint8_t *buffer = tigrReadFile(fname,NULL);
	if (buffer!=NULL)
	{
		BMPHeader_t		*bmp_header=NULL;
		bmp_header = (BMPHeader_t*)&buffer[0];
		uint8_t *colors = &buffer[0]+sizeof(BMPHeader_t);

		printf("bpp %d pack %d\n",bmp_header->bitsPerPixel,bmp_header->compression);

		if ((bmp_header->bitsPerPixel<4) || (bmp_header->bitsPerPixel>8)) 
		{
			printf("4bpp or 8bpp only\n");
			return 0;
		}
//		image->colors = (uint8_t*)malloc(256*4);
//		memcpy(image->colors,colors,256*4);
		Tigr *bmp = tigrBitmap(bmp_header->width, bmp_header->height);

		for (int q=0;q<256;q++)
		{
			bmp->clut[(q*3)]=colors[(q*4)+2];
			bmp->clut[(q*3)+1]=colors[(q*4)+1];
			bmp->clut[(q*3)+2]=colors[(q*4)+0];
		}

		int stride = ((bmp_header->width * bmp_header->bitsPerPixel / 8) + 3) & ~3;
		if (bmp_header->bitsPerPixel==4)
		{
			uint8_t *src = &buffer[bmp_header->bitmapDataOffset];

			for (uint32_t y=0;y<bmp_header->height;y++)
			{
				for (uint32_t x=0;x<bmp_header->width;x++)
				{
					uint8_t byte=src[(x>>1)+((bmp_header->height-1-y)*(bmp_header->width>>1))];
					if ((x&1)==0) byte>>=4;					
					bmp->pix[x+(y*bmp_header->width)]=byte&0xf;
				}
			}
		}
		else 
		{
			uint8_t *src = &buffer[bmp_header->bitmapDataOffset];
			for (uint32_t y=0;y<bmp_header->height;y++)
			{
				for (uint32_t x=0;x<bmp_header->width;x++)
				{
					uint8_t byte=src[x+((bmp_header->height-1-y)*bmp_header->width)];
					bmp->pix[x+(y*bmp_header->width)]=byte;
				}
			}
		}
		return bmp;
	}
	return NULL;
}
