
#include "tigr.h"

#include "tigr_upscale_gl_vs.h"
#include "tigr_upscale_gl_fs.h"

#include "tigr_bitmaps.c"
#include "tigr_print8bit.c"
#include "tigr_utils.c"
#include "tigr_win.c"
#include "tigr_osx.c"
#include "tigr_linux.c"
#include "tigr_gl.c"
